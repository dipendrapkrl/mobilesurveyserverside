<?php

Class Json_util {

    public function get_json($formDataXml){//="1:^2:^3:^4:^5:^6:^7:^8:^9:^10:^11:^12:^13:^14:^15:^16:^17:^18:^19:^20:^21:^22:") {

       $formName ="1";// $values_array[1];

        require 'xformparse.php';
        $dom = new MyDOMDocument;


        $xml_string = $formDataXml;//$this->getXmlString($formName);
        if (is_null($xml_string)) {
            echo "Couldnt find xml string from database";
            die();
        }

        $dom->loadXML($xml_string);

        $array_data = $dom->toArray();
       

        $data_key = $array_data["h:html"]["h:head"]["model"]["instance"]["data"];
        $data_key_truncated = array_splice($data_key, 5);
        $data_key_only = array_splice($data_key_truncated, 0, -4);

        $numeric_key = array();

        $numeric_key[] = "";
        foreach ($data_key_only as $key => $value) {
            $numeric_key[] = $key;
        }

        $data_values = $array_data["h:html"]["h:body"];

        $data_values_input = $data_values['input'];
        $data_values_upload = $data_values['upload'];
        $data_values_select = $data_values['select'];
        $data_values_select1 = $data_values['select1'];


        $input_array = $this->getInputArray($data_values_input);
        $upload_array = $this->getUploadArray($data_values_upload);
        $select1_array = $this->getSelect1Array($data_values_select1);
        $select_array = $this->getSelectArray($data_values_select);



       
      $finalArray= $this->getFinalArray($numeric_key,$input_array, $upload_array, $select1_array, $select_array);
    
     // echo "<pre>";
     /// print_r($finalArray);
	  
	  return $finalArray;
      //print_r(json_encode($finalArray));
     // print_r(json_decode(json_encode($finalArray),TRUE));
    }
       
    

    private function getXmlString($formName) {

        require_once 'database.php';
        $result = mysql_query("SELECT xml_form FROM form WHERE id = 1 LIMIT 0,1");

      
        while ($row = mysql_fetch_array($result)) {
            $string =$row['xml_form'] ;//. " " . $row['LastName'];
         
            break;
        }

        mysql_close($con);




        return $string;
    }

				
    private function getFinalArray($numeric_key, $input_array, $upload_array, $select1_array, $select_array) {

    
       
       $count= 0;
        $array_data =array();
       foreach ($numeric_key as $key => $value) {
         
           if($count==0){
               $count++;
              
               continue;;
           }
           
           if($this->isInputType($input_array, $value)){
               $array_data[]= $input_array[$value];
               
               
           }else if($this->isUploadType($upload_array, $value)){
               $array_data[]= $upload_array[$value];
               
               
           }if($this->isSelect1Type($select1_array, $value)){
               $array_data[]= $select1_array[$value];
               
               
           }if($this->isSelectType($select_array, $value)){
               $array_data[]= $select_array[$value];
               
               
           }
           
           
           
       }
      
       
        return $array_data;
    }

    function isSelectType($select_array, $numeric_key) {
        foreach ($select_array as $key => $value) {
            if ($key == $numeric_key)
                return true;
        }

        return false;
    }

    function isInputType($input_array, $numeric_key) {
        foreach ($input_array as $key => $value) {
            if ($key == $numeric_key)
                return true;
        }

        return false;
    }
    function isUploadType($upload_array, $numeric_key) {
        foreach ($upload_array as $key => $value) {
            if ($key == $numeric_key)
                return true;
        }

        return false;
    }

    function isSelect1Type($select1_array, $numeric_key) {
        foreach ($select1_array as $key => $value) {
            if ($key == $numeric_key)
                return true;
        }

        return false;
    }

    function getInputArray($data_values_input) {

        $index = 1;
        foreach ($data_values_input as $key => $value) {
            if (is_array($value)) {
                $array_data[substr($value['@ref'], 6)]['input'] = $index++;

                if (array_key_exists('question', $value)) {

                    $array_data[substr($value['@ref'], 6)]['question'] = $value['question'];
                }
                if (array_key_exists('required', $value)) {

                    $array_data[substr($value['@ref'], 6)]['required'] = $value['required'];
                }
            }
        }
        return $array_data;
    }

    function getselect1Array($data_values_select1) {

        foreach ($data_values_select1 as $key => $value) {
            if (is_array($value)) {
                $item = $value['item'];
                $name = substr($value['@ref'], 6);


                if (is_array($item)) {
                    foreach ($item as $key => $val) {
                        $key1 = substr($val['label']['@ref'], -4, 2);

                        if (!is_numeric($key1)) {

                            $key1 = substr($val['label']['@ref'], -3, 1);
                        }

                        $val1 = $val['value'];

                        $array_data[$name]['select1'][$key1] = $val1;
                    }
                    if (array_key_exists('question', $value)) {

                        $array_data[$name]['question'] = $value['question'];
                    }
                    if (array_key_exists('required', $value)) {

                        $array_data[$name]['required'] = $value['required'];
                    }
                }
            }
        }

        return $array_data;
    }

    function getselectArray($data_values_select) {

        foreach ($data_values_select as $key => $value) {
            if (is_array($value)) {
                $item = $value['item'];
                $name = substr($value['@ref'], 6);


                if (is_array($item)) {
                    foreach ($item as $key => $val) {
                        $key1 = substr($val['label']['@ref'], -4, 2);

                        if (!is_numeric($key1)) {

                            $key1 = substr($val['label']['@ref'], -3, 1);
                            ;
                        }

                        $val1 = $val['value'];

                        $array_data[$name]['selectM'][$key1] = $val1;
                    }
                    if (array_key_exists('question', $value)) {

                        $array_data[$name]['question'] = $value['question'];
                    }
                    if (array_key_exists('required', $value)) {

                        $array_data[$name]['required'] = $value['required'];
                    }
                }
            }
        }

        return $array_data;
    }

    function getUploadArray($data_values_upload) {
        foreach ($data_values_upload as $key => $value) {
            if (array_key_exists('question', $value)) {

                $array_data[substr($value['@ref'], 6)]['question'] = $value['question'];
            }
            if (array_key_exists('required', $value)) {

                $array_data[substr($value['@ref'], 6)]['required'] = $value['required'];
            }
            if (is_array($value)) {
                $array_data[substr($value['@ref'], 6)]['textArea'] = '';
            }
        }
        if (is_array($array_data)) {
            return $array_data;
        }else
            return null;
    }

    function getArray($values) {


        foreach ($values as $key => $value) {

            $part = explode(KEY_VALUE_SEPARATOR, $value);

            $select = explode(MULTI_SELECT_VALUE_SEPERATOR, $part[1]);

            if (count($select) > 1) {
                $val[$part[0]] = $select;
            } else {
                $val[$part[0]] = $part[1];
            }
        }

        return $val;
    }

}
