-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2012 at 07:27 AM
-- Server version: 5.5.24-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mobilesurvey`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_info_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_answer_1_idx` (`user_info_id`),
  KEY `fk_answer_2_idx` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `xml_form` text NOT NULL,
  `weightage` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `organization_id` (`organization_id`),
  KEY `fk_form_1_idx` (`organization_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `organization_id`, `start_date`, `end_date`, `xml_form`, `weightage`, `is_active`) VALUES
(1, 1, '2012-09-16 00:00:00', '2012-09-30 00:00:00', '<?xml version="1.0" encoding="UTF-8"?>\r\n<h:html xmlns="http://www.w3.org/2002/xforms" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:jr="http://openrosa.org/javarosa">\r\n  <h:head>\r\n    <h:title>General Event</h:title>\r\n    <model>\r\n<!-- revised from NewEvent form to include a new field A for form identification  -->\r\n      <instance>\r\n        <data id="General-Event" category="PublicEngagement" tag="New" revision_date="2012-06-04">\r\n          <device_id/>\r\n          <subscriber_id/>\r\n          <sim_id/>\r\n          <Ph/>\r\n          <A>EVN</A>\r\n          <EE/>\r\n          <FF/>\r\n          <GG/>\r\n          <Z/>\r\n          <Z1/>\r\n          <Z2/>\r\n          <Z3/>\r\n          <Z4/>\r\n          <TY/>\r\n          <B/>\r\n          <C/>\r\n          <D/>\r\n          <E/>\r\n          <F/>\r\n          <G/>\r\n          <H/>\r\n          <I/>\r\n          <J/>\r\n          <K/>\r\n          <L/>\r\n          <M/>\r\n          <M1/>\r\n          <N/>\r\n          <O/>\r\n          <P/>\r\n          <Q/>\r\n          <R/>\r\n          <S/>\r\n          <T/>\r\n          <U/>\r\n          <V/>\r\n          <W/>\r\n          <X/>\r\n          <Y/>\r\n          <AA/>\r\n          <BB/>\r\n          <CC/>\r\n          <DD/>\r\n          <HH/>\r\n        </data>\r\n      </instance>\r\n     \r\n     \r\n    </model>\r\n  </h:head>\r\n  <h:body>\r\n    <input ref="/data/A">\r\n    \r\n	<question>This is question</question>\r\n<required/>\r\n    </input>\r\n    <upload ref="/data/EE" mediatype="image/*">\r\n     <question>This is question</question>\r\n<required/>\r\n    </upload>\r\n    <upload ref="/data/FF" mediatype="audio/*">\r\n      <question>This is question</question>\r\n    </upload>\r\n    <upload ref="/data/GG" mediatype="video/*">\r\n      <question>This is question</question>\r\n    </upload>\r\n    <select1 ref="/data/Z">\r\n<question>This is question</question>\r\n      \r\n      <item>\r\n        <label ref="jr:itext(''/data/Z:option0'')"/>\r\n        <value>GPS</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/Z:option1'')"/>\r\n        <value>LL</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/Z:option2'')"/>\r\n        <value>Txt</value>\r\n      </item>\r\n    </select1>\r\n\r\n    <input ref="/data/Z1">\r\n     <question>This is question</question>\r\n<required/>\r\n    </input>\r\n    <input ref="/data/Z2">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/Z3">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/Z4">\r\n      <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/TY">\r\n      \r\n<question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/TY:option0'')"/>\r\n        <value>TY1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/TY:option1'')"/>\r\n        <value>TY2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/TY:option2'')"/>\r\n        <value>TY3</value>\r\n      </item>\r\n    </select1>\r\n    <select1 ref="/data/B">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option0'')"/>\r\n        <value>B1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option1'')"/>\r\n        <value>B2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option2'')"/>\r\n        <value>B3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option3'')"/>\r\n        <value>B4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option4'')"/>\r\n        <value>B5</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/B:option5'')"/>\r\n        <value>B6</value>\r\n      </item>\r\n    </select1>\r\n    <select1 ref="/data/C">\r\n     <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/C:option0'')"/>\r\n        <value>C1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/C:option1'')"/>\r\n        <value>C2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/C:option2'')"/>\r\n        <value>C3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/C:option3'')"/>\r\n        <value>C4</value>\r\n      </item>\r\n    </select1>\r\n    <select ref="/data/D">\r\n     <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option0'')"/>\r\n        <value>D1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option1'')"/>\r\n        <value>D2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option2'')"/>\r\n        <value>D3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option3'')"/>\r\n        <value>D4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option4'')"/>\r\n        <value>D5</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option5'')"/>\r\n        <value>D6</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option6'')"/>\r\n        <value>D7</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option7'')"/>\r\n        <value>D8</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option8'')"/>\r\n        <value>D9</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/D:option9'')"/>\r\n        <value>D10</value>\r\n      </item>\r\n    </select>\r\n    <input ref="/data/E">\r\n      <question>This is question</question>\r\n    </input>\r\n    <select ref="/data/F">\r\n<question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/F:option0'')"/>\r\n        <value>F1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/F:option1'')"/>\r\n        <value>F2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/F:option2'')"/>\r\n        <value>F3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/F:option3'')"/>\r\n        <value>F4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/F:option4'')"/>\r\n        <value>F5</value>\r\n      </item>\r\n    </select>\r\n    <input ref="/data/G">\r\n     <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/H">\r\n      <label ref="jr:itext(''/data/H:label'')"/>\r\n      <hint ref="jr:itext(''/data/H:hint'')"/>\r\n      <item>\r\n        <label ref="jr:itext(''/data/H:option0'')"/>\r\n        <value>H1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/H:option1'')"/>\r\n        <value>H2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/H:option2'')"/>\r\n        <value>H3</value>\r\n      </item>\r\n    </select1>\r\n    <input ref="/data/I">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/J">\r\n      <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/K">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/K:option0'')"/>\r\n        <value>K1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/K:option1'')"/>\r\n        <value>K2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/K:option2'')"/>\r\n        <value>K3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/K:option3'')"/>\r\n        <value>K4</value>\r\n      </item>\r\n    </select1>\r\n    <select ref="/data/L">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option0'')"/>\r\n        <value>L1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option1'')"/>\r\n        <value>L2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option2'')"/>\r\n        <value>L3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option3'')"/>\r\n        <value>L4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option4'')"/>\r\n        <value>L5</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/L:option5'')"/>\r\n        <value>L6</value>\r\n      </item>\r\n    </select>\r\n    <input ref="/data/M">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/M1">\r\n      <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/N">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/N:option0'')"/>\r\n        <value>N1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/N:option1'')"/>\r\n        <value>N2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/N:option2'')"/>\r\n        <value>N3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/N:option3'')"/>\r\n        <value>N4</value>\r\n      </item>\r\n    </select1>\r\n    <input ref="/data/O">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/P">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/Q">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/R">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/S">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/T">\r\n     <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/U">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option0'')"/>\r\n        <value>U1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option1'')"/>\r\n        <value>U2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option2'')"/>\r\n        <value>U3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option3'')"/>\r\n        <value>U4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option4'')"/>\r\n        <value>U5</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/U:option5'')"/>\r\n        <value>U6</value>\r\n      </item>\r\n    </select1>\r\n    <input ref="/data/V">\r\n      <question>This is question</question>\r\n    </input>\r\n    <select1 ref="/data/W">\r\n      <question>This is question</question>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option0'')"/>\r\n        <value>W1</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option1'')"/>\r\n        <value>W2</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option2'')"/>\r\n        <value>W3</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option3'')"/>\r\n        <value>W4</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option4'')"/>\r\n        <value>W5</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option5'')"/>\r\n        <value>W6</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option6'')"/>\r\n        <value>W7</value>\r\n      </item>\r\n      <item>\r\n        <label ref="jr:itext(''/data/W:option7'')"/>\r\n        <value>W8</value>\r\n      </item>\r\n    </select1>\r\n    <input ref="/data/X">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/Y">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/AA">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/BB">\r\n      <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/CC">\r\n     <question>This is question</question>\r\n    </input>\r\n    <input ref="/data/HH">\r\n     <question>This is question</question>\r\n    </input>\r\n  </h:body>\r\n</h:html>', 150, 1);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `name`, `address`, `phone_no`, `email`) VALUES
(1, 'Hackathon ', 'Nepal', '984000000', 'hackathon@hackathon.org');

-- --------------------------------------------------------

--
-- Table structure for table `user_acc_info`
--

CREATE TABLE IF NOT EXISTS `user_acc_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_info_id` int(11) NOT NULL,
  `no_of_submitted_form` int(11) NOT NULL,
  `current_points` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_acc_info_1_idx` (`user_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `fb_id` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `registered_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_fb_id` (`fb_id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `fk_answer_1` FOREIGN KEY (`user_info_id`) REFERENCES `user_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_answer_2` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form`
--
ALTER TABLE `form`
  ADD CONSTRAINT `fk_form_1` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_acc_info`
--
ALTER TABLE `user_acc_info`
  ADD CONSTRAINT `fk_user_acc_info_1` FOREIGN KEY (`user_info_id`) REFERENCES `user_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
