<?xml version="1.0" encoding="UTF-8"?>
<h:html xmlns="http://www.w3.org/2002/xforms" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:jr="http://openrosa.org/javarosa">
  <h:head>
    <h:title>General Event</h:title>
    <model>
<!-- revised from NewEvent form to include a new field A for form identification  -->
      <instance>
        <data id="General-Event" category="PublicEngagement" tag="New" revision_date="2012-06-04">
          <device_id/>
          <subscriber_id/>
          <sim_id/>
          <Ph/>
          <A>EVN</A>
          <EE/>
          <FF/>
          <GG/>
          <Z/>
          <Z1/>
          <Z2/>
          <Z3/>
          <Z4/>
          <TY/>
          <B/>
          <C/>
          <D/>
          <E/>
          <F/>
          <G/>
          <H/>
          <I/>
          <J/>
          <K/>
          <L/>
          <M/>
          <M1/>
          <N/>
          <O/>
          <P/>
          <Q/>
          <R/>
          <S/>
          <T/>
          <U/>
          <V/>
          <W/>
          <X/>
          <Y/>
          <AA/>
          <BB/>
          <CC/>
          <DD/>
          <HH/>
        </data>
      </instance>
     
     
    </model>
  </h:head>
  <h:body>
    <input ref="/data/A">
    
	<question>This is question</question>
<required/>
    </input>
    <upload ref="/data/EE" mediatype="image/*">
     <question>This is question</question>
<required/>
    </upload>
    <upload ref="/data/FF" mediatype="audio/*">
      <question>This is question</question>
    </upload>
    <upload ref="/data/GG" mediatype="video/*">
      <question>This is question</question>
    </upload>
    <select1 ref="/data/Z">
<question>This is question</question>
      
      <item>
        <label ref="jr:itext('/data/Z:option0')"/>
        <value>GPS</value>
      </item>
      <item>
        <label ref="jr:itext('/data/Z:option1')"/>
        <value>LL</value>
      </item>
      <item>
        <label ref="jr:itext('/data/Z:option2')"/>
        <value>Txt</value>
      </item>
    </select1>

    <input ref="/data/Z1">
     <question>This is question</question>
<required/>
    </input>
    <input ref="/data/Z2">
      <question>This is question</question>
    </input>
    <input ref="/data/Z3">
      <question>This is question</question>
    </input>
    <input ref="/data/Z4">
      <question>This is question</question>
    </input>
    <select1 ref="/data/TY">
      
<question>This is question</question>
      <item>
        <label ref="jr:itext('/data/TY:option0')"/>
        <value>TY1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/TY:option1')"/>
        <value>TY2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/TY:option2')"/>
        <value>TY3</value>
      </item>
    </select1>
    <select1 ref="/data/B">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/B:option0')"/>
        <value>B1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/B:option1')"/>
        <value>B2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/B:option2')"/>
        <value>B3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/B:option3')"/>
        <value>B4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/B:option4')"/>
        <value>B5</value>
      </item>
      <item>
        <label ref="jr:itext('/data/B:option5')"/>
        <value>B6</value>
      </item>
    </select1>
    <select1 ref="/data/C">
     <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/C:option0')"/>
        <value>C1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/C:option1')"/>
        <value>C2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/C:option2')"/>
        <value>C3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/C:option3')"/>
        <value>C4</value>
      </item>
    </select1>
    <select ref="/data/D">
     <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/D:option0')"/>
        <value>D1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option1')"/>
        <value>D2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option2')"/>
        <value>D3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option3')"/>
        <value>D4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option4')"/>
        <value>D5</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option5')"/>
        <value>D6</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option6')"/>
        <value>D7</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option7')"/>
        <value>D8</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option8')"/>
        <value>D9</value>
      </item>
      <item>
        <label ref="jr:itext('/data/D:option9')"/>
        <value>D10</value>
      </item>
    </select>
    <input ref="/data/E">
      <question>This is question</question>
    </input>
    <select ref="/data/F">
<question>This is question</question>
      <item>
        <label ref="jr:itext('/data/F:option0')"/>
        <value>F1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/F:option1')"/>
        <value>F2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/F:option2')"/>
        <value>F3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/F:option3')"/>
        <value>F4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/F:option4')"/>
        <value>F5</value>
      </item>
    </select>
    <input ref="/data/G">
     <question>This is question</question>
    </input>
    <select1 ref="/data/H">
      <label ref="jr:itext('/data/H:label')"/>
      <hint ref="jr:itext('/data/H:hint')"/>
      <item>
        <label ref="jr:itext('/data/H:option0')"/>
        <value>H1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/H:option1')"/>
        <value>H2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/H:option2')"/>
        <value>H3</value>
      </item>
    </select1>
    <input ref="/data/I">
     <question>This is question</question>
    </input>
    <input ref="/data/J">
      <question>This is question</question>
    </input>
    <select1 ref="/data/K">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/K:option0')"/>
        <value>K1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/K:option1')"/>
        <value>K2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/K:option2')"/>
        <value>K3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/K:option3')"/>
        <value>K4</value>
      </item>
    </select1>
    <select ref="/data/L">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/L:option0')"/>
        <value>L1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/L:option1')"/>
        <value>L2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/L:option2')"/>
        <value>L3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/L:option3')"/>
        <value>L4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/L:option4')"/>
        <value>L5</value>
      </item>
      <item>
        <label ref="jr:itext('/data/L:option5')"/>
        <value>L6</value>
      </item>
    </select>
    <input ref="/data/M">
     <question>This is question</question>
    </input>
    <input ref="/data/M1">
      <question>This is question</question>
    </input>
    <select1 ref="/data/N">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/N:option0')"/>
        <value>N1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/N:option1')"/>
        <value>N2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/N:option2')"/>
        <value>N3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/N:option3')"/>
        <value>N4</value>
      </item>
    </select1>
    <input ref="/data/O">
      <question>This is question</question>
    </input>
    <input ref="/data/P">
     <question>This is question</question>
    </input>
    <input ref="/data/Q">
     <question>This is question</question>
    </input>
    <input ref="/data/R">
     <question>This is question</question>
    </input>
    <input ref="/data/S">
     <question>This is question</question>
    </input>
    <input ref="/data/T">
     <question>This is question</question>
    </input>
    <select1 ref="/data/U">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/U:option0')"/>
        <value>U1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/U:option1')"/>
        <value>U2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/U:option2')"/>
        <value>U3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/U:option3')"/>
        <value>U4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/U:option4')"/>
        <value>U5</value>
      </item>
      <item>
        <label ref="jr:itext('/data/U:option5')"/>
        <value>U6</value>
      </item>
    </select1>
    <input ref="/data/V">
      <question>This is question</question>
    </input>
    <select1 ref="/data/W">
      <question>This is question</question>
      <item>
        <label ref="jr:itext('/data/W:option0')"/>
        <value>W1</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option1')"/>
        <value>W2</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option2')"/>
        <value>W3</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option3')"/>
        <value>W4</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option4')"/>
        <value>W5</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option5')"/>
        <value>W6</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option6')"/>
        <value>W7</value>
      </item>
      <item>
        <label ref="jr:itext('/data/W:option7')"/>
        <value>W8</value>
      </item>
    </select1>
    <input ref="/data/X">
      <question>This is question</question>
    </input>
    <input ref="/data/Y">
      <question>This is question</question>
    </input>
    <input ref="/data/AA">
      <question>This is question</question>
    </input>
    <input ref="/data/BB">
      <question>This is question</question>
    </input>
    <input ref="/data/CC">
     <question>This is question</question>
    </input>
    <input ref="/data/HH">
     <question>This is question</question>
    </input>
  </h:body>
</h:html>
